# FROM python:3.9.1-alpine3.12
FROM python:3.9.1-slim

RUN apt-get update -qq \
  && apt-get install -y \
    ghostscript python3-tk ffmpeg libsm6 libxext6 libgl1-mesa-dev \
  && apt-get clean \
  && rm -rf /var/lib/apt/lists/* \
# RUN apk --update --no-cache add gcc python3-dev musl-dev ghostscript python3-tkinter \
#   && pip install --upgrade pip \
  # && pip install --no-cache numpy \
  && pip install --no-cache "camelot-py[cv]"
WORKDIR /home
