# camelot

[Camelot: PDF Table Extraction for Humans](https://camelot-py.readthedocs.io)  
PDF のテーブルっぽいみためになってるところを html にして保存してくれるツール  

…のインストールがうまく行かなかったので docker のコンテナ内でインストールした  

# usage

```shell
docker run --rm -it -v $(pwd):/home registry.gitlab.com/yt-lib/camelot-py:latest bash
```


# dev

## build

```shell
docker build -t registry.gitlab.com/yt-lib/camelot-py:latest .
```

## push

```shell
docker push registry.gitlab.com/yt-lib/camelot-py:latest
```
